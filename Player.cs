using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour{
		public AudioClip clip;
		public AudioClip death;
		public AudioClip deathaki;
		public AudioClip hurtso;
		public AudioClip jumpso;
		public AudioClip electrocutedso;
		public AudioClip transformacio;
		private CharacterController _controller;
		private GameMaster gm;
		private Checkpoint ch;
		private Rigidbody2D _myRigidbody;
		private float Timer;	
		private float Timerviscos;
		private float Timerveri;
		private float timernorun;
		private float move = 0;
		public bool _facingRight;
		private bool flag;
		private bool terrastun = false;
		private int cont;
		private Cartell ca; 
		private Dialogos dg;
		private bool bossactiu = false;
		//private bool quiet=false;
//		private float tempsmal = 0f;
		private float timeranim = 0f;
		private float timerplatform = 0f;
		private bool tocat= false;	
		private bool climb=false;
		private bool viscos;
		private bool veri;
		private bool landing;
		public bool cloudland;
		private bool bloqueig;
		private bool jump;
		private bool bloqueigsalt = false;
		public bool platform_best = false;
		private bool electrocuted ;
		private bool punxa= false;
		private bool flagso=true;
//		private bool estatanterior =false;
		private float contador; //per al veri de la serp
		public List<Transform> serps;
		

		private Transform myTransform;
		public bool IsDead { get; private set;}
		public GameObject personatge;
		public GameObject p_trans;
		public int notrans;
		public Animator _anim;
		public float speed;
		public Transform grounder;
		public Transform electric;
		public Transform isbucleviril;
		public Transform syntax;
		public float radiuss;
		public LayerMask ground;
		public bool isGrounded = false;
		public bool isbucle = false;
		public bool syntaxerror = false;
		public Transform platform;
		public float timer = 0.1f;
		public bool agafat;
		public bool once2=true;
//		float timebarrido=0.5f;
		private Vector3 _movingVector = new Vector3(1, 0, 0);
		private Health h;
		private PauseMenu pm;	
		private PlayerHealth ph;
		private UI ui;
		private transformations t;
		private LvlManager lvlm;
	
		void Awake(){

		}



		// Use this for initialization
		void Start ()
		{
			lvlm= GameObject.Find("LvlManager").GetComponent<LvlManager>();
			ui = GameObject.Find("UI").GetComponent<UI>();
			if (GameObject.Find ("Player") != null) {
				ph = GameObject.Find ("Player").GetComponent<PlayerHealth> ();
			}
			if (GameObject.Find ("Player") == null) {
				ph = GameObject.Find ("Player_trans").GetComponent<PlayerHealth> ();
			}
			pm = GameObject.Find("GameMaster").GetComponent<PauseMenu>();
			contador=1;
			myTransform = transform;
			GameObject[] go5 = GameObject.FindGameObjectsWithTag ("Serp");
			foreach (GameObject enemy in go5)
			AddTarget (enemy.transform);
			t = GameObject.Find("LvlManager").GetComponent<transformations>();
			gm = GameObject.Find("GameMaster").GetComponent<GameMaster>();
			if(gm.DebugSpawn!= null){
				ch = GameObject.Find ("DebugSpawn").GetComponent<Checkpoint> ();
			}
			else{
				ch = GameObject.Find ("Startpoint").GetComponent<Checkpoint> ();
			}
			_myRigidbody = this.rigidbody2D;
			_anim = GetComponent<Animator>();
			agafat = false;
			Timer = 0.5f;
			p_trans = ch._pc_trans;
			personatge = ch._pc;
			if(personatge!=null){
				if(personatge.activeInHierarchy==true) notrans=0;
				if(personatge.activeInHierarchy==false) notrans=1;
			}
			SortTargetsByDistance();
			viscos = false;
			Timerviscos=0;
			landing = true;
			electrocuted = false;
			timerplatform = 0f;
			h = GameObject.Find("Health").GetComponent<Health>();
			GameObject.Find("Startpoint").SendMessage("ActivateTargetting");
			GameObject.Find("Camera").SendMessage("ActiveFlag");
			dg= GameObject.Find("GameMaster").GetComponent<Dialogos>();
	}	
	
		// Update is called once per frame
		void Update ()	{
			if(isGrounded){
				audio.Pause ();
				flagso=true;
			}
			_anim = GetComponent<Animator>();
			if(IsDead) transform.parent = null;
			if(timeranim>0) timeranim -= Time.deltaTime;
			if(timeranim<0) timeranim =0;
			if(timernorun>0) timernorun -= Time.deltaTime;
			if(timernorun<0) {
				timernorun =0;
				jump=false;
				bloqueig=false;
			}	
			if(timerplatform>0) timerplatform -= Time.deltaTime;
			if(timerplatform<0) timerplatform =0;
			if(Timerviscos>0) Timerviscos -= Time.deltaTime;
			if(Timerviscos<0) Timerviscos =0;
			if(Timerviscos == 0){// calcula el temps que l'animacio de mal esta activa
				viscos=false;

			}
			if(Timerveri>0) Timerveri -= Time.deltaTime;
			if(Timerveri<0) Timerveri =0;
			
			//part del codi que mira en quina posicio/transformacio esta el personatge.
			if(personatge!=null){
				if(personatge.activeInHierarchy==true){
					if (personatge.transform.rotation.y == 0)
						_facingRight = true;
					if (personatge.transform.rotation.y == 1)
						_facingRight = false;
				}
				if(personatge.activeInHierarchy==false){
					if (p_trans.transform.rotation.y == 0)
						_facingRight = true;
					if (p_trans.transform.rotation.y == 1)
						_facingRight = false;
				}
			}

			if(isGrounded){
				climb=false;
			}
			
			if(personatge!=null){
				if(personatge.activeInHierarchy==true) notrans=0;
				if(personatge.activeInHierarchy==false) notrans=1;
			}
			//fi dels checks
			if(IsDead){
				_anim.SetBool("mal", false);
				_anim.SetBool("Jump", false);
				_anim.SetFloat("Speed", 0);
				veri=false;
				electrocuted=false;
				viscos = false;
			}
			if(!IsDead){
				//bloqueigsalt=electrocuted;
				_anim.SetBool ("death", false);
				if(Input.GetKeyDown (KeyCode.Space)|| Input.GetButtonDown("Jump")){
					if(_anim.GetBool("mal")==false){
						jump=true;
					}
				}

				syntaxerror = Physics2D.OverlapCircle (syntax.transform.position, 0.05f, ground);
				if(!bloqueig){ 
					move = Input.GetAxisRaw("Horizontal");
				//	Debug.Log (move);
					if(Mathf.Abs (move)<0.2)move = 0;
				}
				//////////
				
				if(syntaxerror && Input.GetAxis("Horizontal") != 0 && jump){

						//Debug.Log ("move=0");
						timernorun=0.1f;

				}
				if(timernorun>0){
					move=0;	
					//Debug.Log ("bloqueig");
					bloqueig=true;
				}

			//	estatanterior=syntaxerror;

				
				if(electrocuted){
					AudioSource.PlayClipAtPoint(electrocutedso, transform.position, 0.05f);
					GameObject elec = Resources.Load("electricity") as GameObject;
					Instantiate(elec, transform.position, transform.rotation);
					GameObject par = Resources.Load("paralyzed") as GameObject;
					Instantiate(par, electric.transform.position, electric.transform.rotation);
					electrocuted=false;
					timernorun=0.7f;
				}
				
				if(!isGrounded){
					_anim.SetBool ("PunchRun", false);
					_anim.SetBool ("PunchNow", false);
					_anim.SetBool ("atac2", false);
					_anim.SetBool ("atac3", false);
				}
				if(veri) {
						if(Timerveri==0){
							if(h!=null){
								h.modifyHealth(-5);
							}
							transform.Translate(0, -0.1f, 0);
							Timerveri=1;
							contador++;
						}
						if(contador==10){
							veri=false;
							contador=1;
						}
					}

					if(viscos==true){
						move=move/3;

					}
					if(_anim.GetBool("PunchRun") == true){
						move=move*1.4f;
					}
					if(_anim.GetBool ("PunchJump")==true){
						_anim.SetFloat ("Speed", 0);
					}
					else{
						_anim.SetFloat ("Speed", Mathf.Abs(move));
					}
					
					if(isGrounded){
						_anim.SetBool ("PunchJump", false);
						_anim.SetBool ("Climb", false);
					}
		
						 //definir la variable que quan apretis per moure es mogui horitzontalment
						
						//if(!Input.GetButton("Fire1")) {
							//_anim.SetBool ("Jump", false);
						_myRigidbody.velocity = new Vector2 (move*speed, _myRigidbody.velocity.y);
						//establir la veocitat de x i y
					//}
					if(_facingRight == true && move < 0){ //tot aquest codi es per a que el personatge es mogui cap a la dreta i a l'esquerra i roti 180 graus
						transform.rotation=Quaternion.Euler (transform.rotation.x,180,transform.rotation.z);
						}
					else if (_facingRight == false && move > 0) {
						transform.rotation=Quaternion.Euler (transform.rotation.x,0,transform.rotation.z);
						}
					// aqui fem una rodona, amb overlapcircle, que al colisionar amb el terra fica a true la variable is grounded i junt si apreten espai el personatge salti
					//llavors amb la linia de punch now == false fa que s'activi l'animacio, aixo es fa sobretot pel salt + atac
					isGrounded = Physics2D.OverlapCircle (grounder.transform.position, radiuss, ground);
					isbucle = Physics2D.OverlapCircle (isbucleviril.transform.position, radiuss*2, ground);
					if(isGrounded){jump=false;}
					if(climb){
						_anim.SetBool ("Jump", false);
						_anim.SetFloat ("Speed", 0);
					}
					if(!isGrounded)landing=true;
					if(platform_best)isGrounded=false;
					if (_anim.GetBool ("PunchJump")==false && !climb ){//linia que fa que nomes es fagi l'animacio de salt si hi ha el punch desactivat, per veure l'atac en el salt
						_anim.SetBool ("Jump", !isGrounded);
						if( Input.GetButton("Fire") &&  Input.GetButtonDown("Jump")||  Input.GetButton("Fire1") && Input.GetKeyDown (KeyCode.Space)){
							_anim.SetBool ("Jump", false);
						}
					}
					if(isGrounded && !punxa && !cloudland){
						if(landing && timerplatform == 0){
							GameObject land = Resources.Load("landing") as GameObject;
							Instantiate(land, grounder.transform.position, grounder.transform.rotation);
							landing=false;
						}
					}
					
					//per a que si fa alguna cosa el salt sigui false, TOT AQUEST SEGUIT DE CONDICIONS ESTAN FETES JA QUE ERA LA UNICA MANERA QUE NO ES CLAVES DE TANT EN TANT
					if(_anim.GetBool ("Shoot") == true || _anim.GetBool ("PunchNow") == true || _anim.GetBool ("mal") == true || _anim.GetBool ("Climb") == true){
						_anim.SetBool ("Jump", false);
					
					}
					if(isbucle == true && _anim.GetBool("Jump")==true ){ //linies de codi que serveixen per evitar que el personatge entri en bucle 
						//Debug.Log ("entra mes a fons");
						_myRigidbody.velocity = new Vector2 (0, _myRigidbody.velocity.y-0.01f);
						//timernorun=10.1f;
					}
		
					if(_anim.GetBool ("mal")==true ){
						_anim.SetBool ("PunchRun", false);
					}
				
					if(!bloqueigsalt){
							if(Input.GetKeyDown (KeyCode.Space) || Input.GetButtonDown("Jump") )//si el jugador apreta espai pot saltar amunt
							{	
								if(isGrounded == true){
									if(_anim.GetBool ("mal")==false ){
										AudioSource.PlayClipAtPoint(jumpso, transform.position, 0.05f);
										if(viscos==false){
											//_myRigidbody.AddForce(new Vector2(0, 9500), ForceMode2D.Impulse);
											_myRigidbody.velocity = new Vector2(_myRigidbody.velocity.x,9.5f);
										}
										else if(viscos==true){//fem que si esta viscos es limiti el salt
											_myRigidbody.velocity = new Vector2(_myRigidbody.velocity.x,8f);	
										} 
									}
							}

					}
						

						}
				if (_anim.GetBool ("mal") == true){//per a quan et fan mal que es desactivi la puta animacio
					if(Timer>0) Timer -= Time.deltaTime;
					if(Timer<0) Timer =0;
					if(Timer == 0){// calcula el temps que l'animacio de mal esta activa
						_anim.SetBool ("mal", false);
						Timer = 0.5f;
					}
					
				}
				
				
			if (Input.GetKeyUp (KeyCode.R) && notrans == 1 && t.cantransform || Input.GetButtonDown("Transformacio") && notrans == 1 && t.cantransform) {
				/*personatge.transform.position = p_trans.transform.position;
				personatge.transform.rotation = p_trans.transform.rotation;
				p_trans.SetActive(false);//aixo es el que fa que transformi d'un model a un altre
				personatge.SetActive(true);
				GameObject.Find("LvlManager").SendMessage("desactivatrans1");
				transform.parent = null;
				GameObject.Find("Startpoint").SendMessage("ActivateTargetting");
				GameObject.Find("Camera").SendMessage("ActiveFlag");
				GameObject.Find("LvlManager").SendMessage("notransformat");
				climb=false;
				_anim.speed=1;*/
				_anim.SetBool ("trans", true);
				timernorun=10f;
				bloqueigsalt=true;
			}
			else if (Input.GetKeyUp (KeyCode.R)&& notrans == 0 && t.cantransform || Input.GetButtonDown("Transformacio") && notrans == 0 && t.cantransform) {

				_anim.SetBool ("transesp", true);
			/*	p_trans.transform.position = personatge.transform.position;
				p_trans.transform.rotation = personatge.transform.rotation;
				personatge.SetActive (false);
				p_trans.SetActive (true);
				GameObject.Find("LvlManager").SendMessage("activatrans1");
				transform.parent = null;
				GameObject.Find("Startpoint").SendMessage("ActivateTargetting");
				GameObject.Find("Camera").SendMessage("ActiveFlag");
				GameObject.Find("LvlManager").SendMessage("transformat");
				climb=false;
				_anim.speed=1;*/
				timernorun=10f;
				bloqueigsalt=true;
				
			}
		}
		
	}
	void OnCollisionEnter2D(Collision2D other){//per detectar la plataforma si entra en col·lisio amb un fa alguna cosa
			if(other.gameObject.tag == "Platform"){
				transform.parent = other.transform;
			}
			if(other.gameObject.tag == "punxa"){
				punxa=true;
			}


		

		if (other.gameObject.tag == "Stalactopus") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
			_anim.SetBool("mal", true);
			if(h!=null){
				h.modifyHealth(-2);
			}
			
			
		}
	}
		void OnCollisionStay2D(Collision2D other){//per detectar la plataforma si entra en col·lisio amb un fa alguna cosa
			
			if(other.gameObject.tag == "platform_best"){
				if(Input.GetAxisRaw("Vertical")<0){
					
					_anim.SetBool("Jump", true);
					isGrounded=false;
				}
			}
			if(other.gameObject.name == "Stalactopus"){
				timernorun=0.3f;
			
			}
		
			
		}

		void OnCollisionExit2D(Collision2D other){
			if(other.gameObject.tag == "Platform"){//per quan surtis de la plataforma no es mantingui la posicio de la plataforma
				transform.parent = null;
			}

			if(other.gameObject.tag == "Ladder"){
				_anim.SetBool ("Climb", false);
			}
			if(other.gameObject.tag == "platform_best"){
				platform_best = false;
				timerplatform = 0.17f;
			}
			if(other.gameObject.tag == "punxa"){
				punxa=false;;
			}

		}


		void OnTriggerEnter2D(Collider2D coll){
			if (coll.gameObject.name == "BulletBossBest(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
	
				GameObject.Find("BulletBossBest(Clone)").SendMessage("activebullet");
				timernorun = 2f;
				bloqueigsalt = true;
				move = 0;

			
			}
			if (coll.tag == "cartell") {
				ca = coll.GetComponent<Cartell> ();
			}
			if (coll.tag == "cutscene") {
				coll.gameObject.SendMessage("activarCS");
			}
			if(coll.gameObject.name == "EndDemo"){
				GameObject.Find("lvlmaster").SendMessage("destroy");
				Application.LoadLevel(3);
			}
			if(coll.gameObject.tag == "platform_best"){
				platform_best = true;
			}

			if(coll.gameObject.tag == "Orbe"){
				Destroy(coll);
				Activar1Trans();
			}
			if(coll.gameObject.tag == "cloudland"){
				cloudland = true;
			}
			if (coll.gameObject.name == "BulletSerpentier(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida	
				GameObject go = Resources.Load("veri") as GameObject;
				if(h!=null){
					h.modifyHealth(-20);
				}
				serps.Clear();
				GameObject[] go5 = GameObject.FindGameObjectsWithTag ("Serp");
				foreach (GameObject enemy in go5)
				AddTarget (enemy.transform);
				SortTargetsByDistance();
				//contador=1;
				_anim.SetBool ("mal", true);
				//Debug.Log (serps);
				SortTargetsByDistance();
				Serp ser = serps[0].GetComponent<Serp>();
				if(Timerveri==0){//com que quant succeix esta a 0 s'executa, aixi evitem que si torna a rebre es torni a fer
					Instantiate(go, transform.position, transform.rotation);
					GameObject par = Resources.Load("venom") as GameObject;
					Instantiate(par, electric.transform.position, electric.transform.rotation);
				}
				if(ser.right==true){
					if(_facingRight){
						transform.Translate(+0.3f, 0, 0);
					}
					else{
						transform.Translate(-0.3f, 0, 0);
					}
				}
				else {
					if(_facingRight){
						transform.Translate(-0.3f, 0, 0);
					}
					else{
						transform.Translate(+0.3f, 0, 0);
					}
				}
				veri= true;
				Timerveri=1f;
				Destroy(coll.gameObject);
			}
			if (coll.gameObject.name == "BulleteStalactopus(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				if(h!=null){
					h.modifyHealth(-50);
				}
				_anim.SetBool ("mal", true);
				GameObject par = Resources.Load("viscos") as GameObject;
				Instantiate(par, electric.transform.position, electric.transform.rotation);
				//	Instantiate(bloodPrefab, bloodPoint.transform.position, bloodPoint.transform.rotation);
				//transform.Translate(0, +0.2f, 0);
				viscos=true;
				Timerviscos = 3f;
				//Destroy(coll.gameObject);
			}
			if (coll.gameObject.name == "BulletBoss(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				if(h!=null){
					h.modifyHealth(-50);
				}
				_anim.SetBool ("mal", true);
				//	Instantiate(bloodPrefab, bloodPoint.transform.position, bloodPoint.transform.rotation);
				//transform.Translate(0, +0.2f, 0);
				Destroy(coll.gameObject);
				}

			if (coll.gameObject.name == "BulletBossPeneLlarg(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				if(h!=null){
					h.modifyHealth(-100);
				}
				_anim.SetBool ("mal", true);
				//	Instantiate(bloodPrefab, bloodPoint.transform.position, bloodPoint.transform.rotation);
				//transform.Translate(0, +0.2f, 0);
				
			}
			if (coll.gameObject.name == "BulleteyeGear(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				if(h!=null){
					h.modifyHealth(-50);
				}
				_anim.SetBool ("mal", true);
				electrocuted = true;
				//	Instantiate(bloodPrefab, bloodPoint.transform.position, bloodPoint.transform.rotation);	
				transform.Translate(0, +0.2f, 0);
				Destroy(coll.gameObject);
			}
			if (coll.gameObject.name == "BulletCano(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				if(h!=null){
					h.modifyHealth(-150);
				}
				transform.Translate(0, -0.2f, 0);
				if(_anim.GetBool("Climb")==false){

					_anim.SetBool ("mal",true);
				}
				Destroy(coll.gameObject);
			}
			if (coll.gameObject.name == "BulletCanoFast(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				if(h!=null){
					h.modifyHealth(-150);
				}
				transform.Translate(0, -0.02f, 0);
				if(_anim.GetBool("Climb")==false){
					
					_anim.SetBool ("mal",true);
				}
				Destroy(coll.gameObject);
			}

			if (coll.gameObject.name == "BulletCanoMiddle(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				if(h!=null){
					h.modifyHealth(-150);
				}
				transform.Translate(0, -0.02f, 0);
				if(_anim.GetBool("Climb")==false){
					
					_anim.SetBool ("mal",true);
				}
				Destroy(coll.gameObject);
			}
				
				
		if (coll.gameObject.name == "Power(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				if(h!=null){
					h.modifyHealth(-100);
				}
				transform.parent = coll.transform;
				//quiet=true;

                  }
			if (coll.gameObject.name == "Deathpit") { //detecta el nom del objecta amb el cual collisiona si es Deathpit,
				//respawn(gm._checkpoints[gm._currentCheckpointIndex]);
				if(h!=null){
					h.modifyHealth(-1000);// la funcio respawn
				}
			}

		}
		void OnTriggerStay2D(Collider2D coll){//per lo de les escales en entrar en contacte amb elles pots pujar amunt
			if(coll.gameObject.tag == "cloudland"){
				cloudland = true;
			}
			if (coll.gameObject.tag == "Fantasma") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				_anim.SetBool("mal", true);
				if(h!=null){
					h.modifyHealth(-2);
				}
				
				
			}
			if (coll.tag == "cartell") { 
				if(lvlm.oncecartell[ca.num_cartell]){
					coll.SendMessage("activarcartelltocar");
				}
				if(Input.GetKeyDown (KeyCode.Q)&& !dg.dialegactiu || Input.GetButtonDown("Cartell")&& !dg.dialegactiu){
					if(once2){
						once2=false;
						coll.SendMessage("activarcartellboto");
					}
				}
			}
			if(coll.gameObject.tag == "stun"){
				if(bossactiu){
					if(gm.vidaboss>0){
						if(_anim.GetBool("Jump")==false && terrastun){
							Debug.Log ("entra");
							GameObject.Find("Cama").SendMessage("terratrue");
							terrastun=false;
						}
					}
				}
			}
			if (coll.gameObject.tag == "Serp") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				_anim.SetBool("mal", true);
				if(h!=null){
					h.modifyHealth(-2);
				}
				
				
			}
			
			if(coll.gameObject.tag == "platform_best"){
				if(Input.GetAxisRaw("Vertical")<0){
					
					_anim.SetBool("Jump", true);
					isGrounded=false;
				}
				}
			if (coll.gameObject.name == "BulletBossBest(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				if(h!=null){
					h.modifyHealth(-2);
				}
				_anim.SetBool ("mal", true);
				
				

			}
			if (coll.gameObject.name == "BulletBossPeneLlarg(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
				if(h!=null){
					h.modifyHealth(-1);
				}
				_anim.SetBool ("mal", true);

			}
			
			if (coll.gameObject.tag == "Ladder") {					
					if(!IsDead){
						float moveup = Input.GetAxisRaw("Vertical");
						//_anim.speed = 1;
							if(Input.GetAxisRaw("Horizontal")!=0 && climb){//per tenir en compte el lateral
								_anim.speed = 1;
								_anim.SetBool("climblat",true);
								_anim.SetBool ("Climb", false);
								if(flagso){
									audio.Play();
									flagso=false;
								}
							}
							if(Input.GetAxisRaw("Vertical")!=0 && climb){//per tenir en compte el lateral
								_anim.speed = 1;
								_anim.SetBool("climblat",false);
								_anim.SetBool ("Climb", true);
								if(flagso){
									audio.Play();
									flagso=false;
								}
							}
							if(Mathf.Abs(moveup)>0.4f){
							timeranim = 0.2f;
							moveup = moveup *3;
							climb=true;
							//float estatic = Mathf.Abs(moveup);
							if(_anim.GetBool ("mal")==false){
								_anim.SetBool ("Climb", true);
							}
							_anim.speed=1;
							_myRigidbody.velocity = new Vector2 (_myRigidbody.velocity.x, moveup+0.5f);
						}else if(climb){
							_anim.SetBool("Jump", false);
							moveup=0;
						if(_anim.GetBool("mal")==false  && _anim.GetBool ("Jump")==false && _anim.GetFloat ("Speed") == 0 && _anim.GetBool ("Climb")==true || _anim.GetBool("mal")==false  && _anim.GetBool ("Jump")==false && _anim.GetFloat ("Speed") == 0 && _anim.GetBool ("climblat")==true){
									if(!isGrounded){
										if(timeranim==0){
											if(Input.GetAxisRaw("Horizontal")==0 && Input.GetAxisRaw("Vertical")==0) {
														_anim.speed=0;//per parar l'animacio quant no es mogui, ojo amb els ifs que poden donar problemes
														audio.Pause ();
														flagso=true;
													}
												}
											}
								}
							//_anim.SetBool ("Climb", true);
							_myRigidbody.velocity = new Vector2 (_myRigidbody.velocity.x, moveup+0.5f);
						}
				}
				if(IsDead){
					
					_myRigidbody.velocity = new Vector2 (_myRigidbody.velocity.x, -1.5f);
					_anim.SetBool ("Climb", false);
					_anim.SetBool ("mal", false);
					_anim.speed  = 1;
				}


			}
			if (coll.gameObject.name == "Power(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				

				//quiet=false;
			}
		}
		void OnTriggerExit2D(Collider2D coll){ //quant surt de l'escala
			if (coll.gameObject.name == "BulletBossBest(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				
			
			GameObject.Find("BulletBossBest(Clone)").SendMessage("desactivarbullet");

			}
			if(coll.gameObject.tag == "cloudland"){
				cloudland = false;
				//timerplatform = 0.2f;
			}
			if (coll.tag == "cartell") { 
				ca=null;
			}

			if(coll.gameObject.tag == "platform_best"){
				platform_best = false;
				timerplatform = 0.17f;
			}
			if (coll.gameObject.tag == "Ladder") {					
				    
				_anim.SetBool ("Climb", false);
				_anim.SetBool("climblat",false);
				_anim.speed=1;
				climb=false;
				audio.Pause ();
				flagso=true;
			
			
		}
		if(coll.gameObject.tag == "stun"){
			if(bossactiu){
				if(gm.vidaboss>0){
					GameObject.Find("Cama").SendMessage("terrafalse");
				}
			}
		}
			if (coll.gameObject.name == "Power(Clone)") {	//quan entri amb contacte amb les bales dels monstres aquest li treguin vida				

					transform.parent = null;
			 }
	}
	public void Kill(){
		//_controller.detectCollisions = false;
		//AQUI ANIRIA L'ANIMACIO DE MUERTE MUY MUERTO
	//	collider2D.enabled = false;
		audio.PlayOneShot(deathaki, 0.2f);
		pm.SendMessage("pauseOff");
		_anim.SetBool ("death", true);
		_anim.SetBool ("mal", false);
		IsDead = true;
		transform.parent = null;
		gm.lifes -= 1;
		veri=false;
		viscos=false;
		electrocuted=false;


	}
	
	public void respawn(Checkpoint checkpoint){
		Checkpoint go = checkpoint;		
		/*if (go == null) {
			go = new GameObject("Startpoint");
			go.transform.position= Vector3.zero;
		}*/
		IsDead = false;
		_anim.SetBool("mal", false);
		//_controller.detectCollisions = true;
		collider2D.enabled = true;
		if (GameObject.Find ("Player") != null) {
			GameObject pl = GameObject.Find ("Player");
			pl.transform.position = go.transform.position; //mou el pj al punt de inici	
		}
		if (GameObject.Find ("Player_trans") != null) {
			GameObject pl = GameObject.Find ("Player_trans");
			pl.transform.position = go.transform.position; //mou el pj al punt de inici
		}
		ph.mort = false;
		ui.loading = false;
		//mou el pj al punt de inici
	}

	public Transform ReturnActivePlayer(){
		if(notrans==0)
			return personatge.transform;
		if (notrans == 1)
			return p_trans.transform;
		else
			return null;
	}
	private void SortTargetsByDistance(){ //Ordena la llista de targets segons la distancia amb el jugador
		serps.Sort(delegate(Transform t1, Transform t2){
			return Vector2.Distance(t1.position, myTransform.position).CompareTo(Vector2.Distance(t2.position, myTransform.position));
		});	
	}
	public void AddTarget(Transform enemy){ //afegeix els enemics a la llista de targets
		serps.Add (enemy);
	}

	public void desactivedeath () {
		_anim.SetBool ("death", false);

	}

	void bloqueigboss () {

		timernorun = 2f;
		bloqueigsalt = true;
		move = 0;

	}

	void desbloqueig(){

		bloqueig = false;
		bloqueigsalt = false;
	}
	private void activeLoading(){
		GameObject.Find ("UI").SendMessage ("activateLoading");
	}
	
	private void deactiveLoading(){
		GameObject.Find ("UI").SendMessage ("deactivateLoading");
	}

	public void activartransformacio(){
		bloqueig=false;
		bloqueigsalt=false;
		timernorun=0;
		_anim.SetBool ("trans", false);
		_anim.SetBool ("transesp", false);
		p_trans.transform.position = personatge.transform.position;
		p_trans.transform.rotation = personatge.transform.rotation;
		personatge.SetActive (false);
		p_trans.SetActive (true);
		GameObject.Find("LvlManager").SendMessage("activatrans1");
		transform.parent = null;
		GameObject.Find("Startpoint").SendMessage("ActivateTargetting");
		GameObject.Find("Camera").SendMessage("ActiveFlag");
		GameObject.Find("LvlManager").SendMessage("transformat");
		climb=false;
		_anim.speed=1;

		
	}


	public void desactivartransformacio(){
		bloqueig=false;
		bloqueigsalt=false;
		timernorun=0;
		personatge.transform.position = p_trans.transform.position;
		personatge.transform.rotation = p_trans.transform.rotation;
		p_trans.SetActive(false);//aixo es el que fa que transformi d'un model a un altre
		personatge.SetActive(true);
		GameObject.Find("LvlManager").SendMessage("desactivatrans1");
		transform.parent = null;
		GameObject.Find("Startpoint").SendMessage("ActivateTargetting");
		GameObject.Find("Camera").SendMessage("ActiveFlag");
		climb=false;
		_anim.speed=1;
		GameObject.Find("LvlManager").SendMessage("notransformat");
	
	}
	public void comprovarterra(){

		terrastun=true;
	}

	public void sotransformacio(){
			AudioSource.PlayClipAtPoint(transformacio, transform.position, 0.5f);
		
	}
	public void hurt(){
		AudioSource.PlayClipAtPoint(hurtso, transform.position, 0.5f);
		
	}
	void deathso(){
		AudioSource.PlayClipAtPoint(death, transform.position, 0.1f);

	}
	private void nomove(){
		_myRigidbody.velocity = new Vector2 (0, 0);
		move = 0;
		_anim.SetFloat ("Speed", 0.0F);
		_anim.SetBool ("Jump", false);
		
	}

	public void activarboss(){
		bossactiu=true;
	}

	private void Activar1Trans(){
		_anim.SetBool("transesp", true);
	}

	public void ActivarMana(){
		Debug.Log ("Activo transformacio");
		t.cantransform = true;
	}
}

