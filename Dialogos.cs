﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System;

public class Dialogos : MonoBehaviour {
    public static Dialogos TM;
    public bool cartell;
    public bool isdialeg;
    public float difference = 1;
    public float differencey = 1;
    public string blabla;
    public bool dialegactiu = false;
    public List<string> Cartells;
    public List<string> Sequencia;
    public int i = 0;
    public int si = 0;
    public int sf = 0;
    public float tempsdialegmin = 2;
    public int tempsdialegmax = 7;
    public float tempsentredialegs = 0.5f;
    public int timernorepeat = 0;
    public int timenorepeat = 40;
    public Texture2D quadre;
    public Texture2D Face;
    public bool sequencia = false;
    public bool endsequencia = false;
    public bool endDialeg = false;
    public bool startdialeg = false;
    public List<string> Persona;
    public bool dialegnpc = false;
    public string NPC;
    public string nomString;

    private string _SeqName;
    private bool mirror;
    private JsonData textData;
    private bool pista = false; 
    //private Color colorfont;
    private bool skip = false; 
    private bool locksequencial = false;
    private IEnumerator corutine;
    private bool canskip = false;
    //private int timer = 0;
    //private int ivalor;
    //private float fvalor;
    //private bool drawface = false;
    public List<string> Speakers;
    public Texture2D Face1;
    public Texture2D Face2;
    public Texture2D Face3;
    public Texture2D Face4;
    public Texture2D Face5;
    public Texture2D Face6;

    public Sprite Face1S;
    public Sprite Face2S;
    public Sprite Face3S;
    public Sprite Face4S;
    public Sprite Face5S;
    public Sprite Face6S;

    // Use this for initialization
    void Awake() {
        if (TM == null) {
            DontDestroyOnLoad(gameObject);
            TM = this;
        }
        else if (TM != this) {
            Destroy(gameObject);
        }  
    }

    void Start() {
    }

    // Update is called once per frame
    void Update() {
       

        difference = (Screen.width / 12.8f) / 100;
        differencey = (Screen.height / 7.2f) / 100;
        //fvalor = 80 * difference;
        //ivalor = Mathf.FloorToInt(fvalor);

        if (!sequencia) {//si el event de parlar no es una sequencia
            if (DataBase.db.getZone() != "Menu" && ActiveDialogos.AD !=null) {
                pintaYColoreaTextosCanvas();
            }
            //colorfont = Color.black;
            if (GameObject.Find("Player") != null) {
                if (Input.GetMouseButtonDown(0) && dialegactiu && timernorepeat > timenorepeat) {//si apreta el boto es desactiva el quadre
                    dialegactiu = false;
                    timernorepeat = 0;
                    toggleDialeg();
                }
            }
            if (dialegactiu) {
                //timer++;
                timernorepeat++;
            }
        }

        if (sequencia) { //si el event de parlar es una sequencia el update pasa per aqui
            if (DataBase.db.getZone() != "Menu" && ActiveDialogos.AD != null) {
                pintaYColoreaTextosCanvas();
            }
            if (GameObject.Find("Player") != null) {
                if (Input.GetMouseButtonDown(0) && dialegactiu) {//cuan el jugador pulsa el boto pasa la CSs
                    skip = true;
                }
            }
            if (canskip && skip&& !GameMaster.GM.getPause()) {
                canskip = false;
                skip = false;
                StopCoroutine(corutine);//para la corutina que s'esta executant
                dialegactiu = false; //fa que despareixi el quadre de dialeg
                if (si < sf) {
                    if (Persona[si] != Persona[si + 1]) mirror = !mirror;
                }
                si++; //aumenta el index del array de dialegs
                if (DataBase.db.getZone() == "Biblioteca" && si == 7)if(!DataBase.db.getEnd(1))SoundMaster.instance.PlayClip("Soroll");
                if (si > sf) { //comproba que el index no pasi delultim string d'aquesta sequencia en cas positiu acaba la sequencia
                    endDialeg = true;
                }
                else StartCoroutine(waitbetweendiags());//crida a una pseudocorutina per a emular el temps d'espera entre que es tanca la finestra i s'obre un altre
            }
            if (!endDialeg) {
                blabla = Sequencia[si];//mentre no s'acabi la sequencia va actualitzan el string a pintar amb el index actual
            }
            if (!startdialeg) {//si sequencia esta activa i no ha comencat el dialeg l'activa e incia la corutina
                if (!locksequencial) {
                    if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
                        startdialeg = true;
                        corutine = nextdialegMobile();
                        StartCoroutine(corutine);
                    }
                    else {
                        startdialeg = true;
                        corutine = nextdialeg();
                        StartCoroutine(corutine);
                    }
                }
            }

            if (endDialeg) {//s'ha acabat el dialeg aixi que pasem a canviar el estat del jugador i posem totes les variables realcionades amb la sequencia en false
                toggleDialeg();
                skip = false;
                sequencia = false;
                endDialeg = false;
                startdialeg = false;
                endsequencia = true;
                si = 0;
            }
        }
    }

    /// <summary>
    /// Deprecated
    /// </summary>
    /// 
    /*
    void OnGUI() {//aixo es la part que pinta el quadre de dialeg
        if (dialegactiu) {
            fvalor = 30 * difference;
            ivalor = Mathf.FloorToInt(fvalor);
            GUIStyle centeredTextStyle = new GUIStyle("label");
            centeredTextStyle.normal.textColor = colorfont;
            centeredTextStyle.alignment = TextAnchor.MiddleCenter;
            centeredTextStyle.fontSize = ivalor;
            GUI.DrawTexture(new Rect((200 * difference), (450 * differencey), (1000 * difference), (200 * differencey)), quadre, ScaleMode.StretchToFill, true);
            if (drawface) GUI.DrawTexture(new Rect((900 * difference), (325 * differencey), (400 * difference), (325 * differencey)), Face, ScaleMode.ScaleToFit, true);
            GUILayout.BeginArea(new Rect((250 * difference), (475 * differencey), (700 * difference), (175 * differencey)));
            GUILayout.FlexibleSpace();
            GUILayout.Label(blabla, centeredTextStyle);
            GUILayout.FlexibleSpace();
            GUILayout.EndArea();
        }
    }*/

    private void pintaYColoreaTextosCanvas() {

        //aixo no estic conveçut que funcioni tot lo be que hauria,
        //potser es redundant al tenir el dialegactiu i fer un set true de certes coses, s'haura de investigar més.
        if (mirror) ActiveDialogos.AD.ActiveMirror(true);
        else ActiveDialogos.AD.ActiveMirror(false);

        if (dialegactiu) {
            
            GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
             ActiveDialogos.AD.Active(true);
            //Canvi de imatges
            Image imageCanvas;

            //mirror efect
            if (!mirror)imageCanvas = canvasObject.transform.Find("Dialeg/Portrait").GetComponent<Image>();
            else imageCanvas = canvasObject.transform.Find("Dialeg/Portrait_M").GetComponent<Image>();

            Sprite imatge;
            if (si < Persona.Count) {
                if (Persona[si] == "Character2") {
                    imatge = Face2S;
                }
                else if (Persona[si] == "Character1") {
                    imatge = Face1S;
                }
                else if (Persona[si] == "Character3") {
                    imatge = Face3S;
                }
                else if (Persona[si] == "Character4") {
                    imatge = Face4S;
                }
                else if (Persona[si] == "Character5") {
                    imatge = Face5S;
                }
                else if (Persona[si] == "Character6") {
                    imatge = Face6S;
                }
                else if (Persona[si] == "LeChuck" || Persona[si] == "All") {
                    imatge = Face1S;
                    ActiveDialogos.AD.LeChuck();
                }
                else {
                    imatge = Face1S;
                }
            }
            else {
                imatge = Face1S;
            }
            imageCanvas.sprite = imatge;
            //Canvi de Textos
            Text textCanvas;

            if (!mirror)textCanvas = canvasObject.transform.Find("Dialeg/Quadre/Text").GetComponent<Text>();
            else textCanvas = canvasObject.transform.Find("Dialeg/Quadre_M/Text").GetComponent<Text>();

            textCanvas.text = blabla;
            //Canvi de Nom
            Text nomCanvas;
            if(!mirror) nomCanvas= canvasObject.transform.Find("Dialeg/Quadre/Nom").GetComponent<Text>();
            else nomCanvas = canvasObject.transform.Find("Dialeg/Quadre_M/Nom").GetComponent<Text>();
            if (si < Persona.Count) {
                if (Persona[si] == "LeChuck") {
                    nomCanvas.text = "LeChuck";
                }
                else if (Persona[si] == "All") {
                    if(DataBase.db.getLanguageStr()== "catala") nomCanvas.text = "Tots";
                    if (DataBase.db.getLanguageStr() == "castella") nomCanvas.text = "Todos";
                    if (DataBase.db.getLanguageStr() == "angles") nomCanvas.text = "Everyone";
                }
                else if (Persona[si] == "Character2") {
                    if (DataBase.db.getZone() == "Biblioteca" && _SeqName != "End_1") nomCanvas.text = "???";
                    else if (DataBase.db.getZone() == "Arxiu" && (si < 4 || !DataBase.db.getSeqPuzzle()) && _SeqName != "End_1") nomCanvas.text = "???";
                    else { 
                        if (DataBase.db.getLanguage() == 1) nomCanvas.text = "Julia";
                        else nomCanvas.text = "Julía";

                    }
                }
                else if (Persona[si] == "Character1") {
                    nomCanvas.text = "Alan";
                }
                else if (Persona[si] == "Character3") {
                    if (DataBase.db.getZone() == "Biblioteca" && _SeqName != "End_1") nomCanvas.text = "???";
                    else if (DataBase.db.getZone() == "Arxiu" && (si < 3 || !DataBase.db.getSeqPuzzle()) && _SeqName != "End_1") nomCanvas.text = "???";
                    else {
                        if (DataBase.db.getLanguage() == 1) nomCanvas.text = "María";
                        else nomCanvas.text = "Maria";
                    }
                }
                else if (Persona[si] == "Character4") {
                    if (DataBase.db.getZone() == "Biblioteca" && _SeqName != "End_1") nomCanvas.text = "???";
                    else if (DataBase.db.getZone() == "Arxiu" && (si < 2 || !DataBase.db.getSeqPuzzle()) && _SeqName != "End_1") nomCanvas.text = "???";
                    else {
                        if (DataBase.db.getLanguage() == 1) nomCanvas.text = "Éric";
                        else nomCanvas.text = "Eric";
                    }
                }
                else if (Persona[si] == "Character5" || Persona[si] == "Character6") {
                    if (DataBase.db.getZone() == "Biblioteca" && _SeqName != "End_1") nomCanvas.text = "???";
                    else if (DataBase.db.getZone() == "Arxiu" && (si < 1 || !DataBase.db.getSeqPuzzle()) && _SeqName != "End_1") nomCanvas.text = "???";
                    else {
                        if (DataBase.db.getLanguage() == 1) nomCanvas.text = "Ramón";
                        else nomCanvas.text = "Ramon";
                    }
                }
                else {
                    nomCanvas.text = "Alan";
                }
            }
            else {
                nomCanvas.text = "Alan";
            }
        }
        else {
                ActiveDialogos.AD.Active(false);
        }
    }

    private IEnumerator waitbetweendiags() {//corutina per a emular el temps d'espera entre finestres, espera el temps i crida un altre vegada la corutina de dialeg sequencial
        yield return new WaitForSeconds(tempsentredialegs);
                if (!locksequencial) {
                    if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
                        dialegactiu = true;
                        corutine = nextdialegMobile();
                        StartCoroutine(corutine);
                    }
                    else {
                        dialegactiu = true;
                        corutine = nextdialeg();
                        StartCoroutine(corutine);
                    }
                }
    }

    private IEnumerator nextdialeg() {//corutina per a fer els dialegs automatics amb X temps entre ells
        skip = false;
        float tempsdialegminT;
        if (pista) tempsdialegminT = 3.0f;
        else if (si == 0) {
            
            tempsdialegminT = 1.5f;
        }
        else tempsdialegminT = tempsdialegmin;

        yield return new WaitForSeconds(tempsdialegminT);//temps minim per a que el jugador no pugi pasar el dialeg
        canskip = true;   
        //a partir d'aqui si el jugador pulsa el boto de saltarho s'ho pot saltar
        yield return new WaitForSeconds(tempsdialegmax);//temps maxim que el dialeg esta en pantalla
        canskip = false;                                 //ja no pot saltarse el dialeg perque.. be no hi ha dialeg en pantalla : )
        dialegactiu = false;                                 //treu el quadre de la pantalla del jugador
        
        if (si < sf) {
            if (Persona[si] != Persona[si + 1]) mirror = !mirror;
        }
        si++;
        if (DataBase.db.getZone() == "Biblioteca" && si == 7) SoundMaster.instance.PlayClip("Soroll");
        if (si > sf) {
            endDialeg = true;
            yield break;
        }

        yield return new WaitForSeconds(tempsentredialegs);//temps d'espera per a que torni a apareixer el dialeg
        if (!locksequencial) {
            dialegactiu = true;
            corutine = nextdialeg();
            StartCoroutine(corutine);
        }                          //crida a la corutina de esperes per al seguent quadre

        yield break;
    }

    private IEnumerator nextdialegMobile() {//corutina per a fer els dialegs automatics amb X temps entre ells
        skip = false;
        float tempsdialegminT;
        if (pista) tempsdialegminT = 3.0f;
        else if (si == 0) {

            tempsdialegminT = 1.5f;
        }
        else tempsdialegminT = tempsdialegmin;

        yield return new WaitForSeconds(tempsdialegminT);//temps minim per a que el jugador no pugi pasar el dialeg
        canskip = true;
        yield break;
    }

    private void toggleDialeg() { //controla el estat del jugador, li treu la posibilitat de moures, la ui etc..
        if (dialegactiu) {
            //desactivar UI
            /*Cursor.lockState = CursorLockMode.Locked;
			UnityEngine.Cursor.visible = false;
			if(!sequencia)Time.timeScale = 0; //en cas de que sigui sequencia no conve parar el temps perque llavors les animacions no pasarien*/
            deactivatemov();
        }
        if (!dialegactiu) {
            //activar UI
            /*Cursor.lockState = CursorLockMode.Locked;
			UnityEngine.Cursor.visible = false;
			if(!sequencia)Time.timeScale = 1;*/
            activatemov();
            //reactivecartellboto(); //canvia la variable booleana per a poder reactivar el cartell 
        }
    }
    private JsonData GetText(string npcTalk) {
        return textData[npcTalk];
    }

    //Carrega la conversa del Npc passat per parametre en el script
    private void populateLists(string npcTalk) {
        _SeqName = npcTalk;
        JsonData JD = GetText(npcTalk);
        if (JD != null) {
            int liniasDialeg = Int32.Parse(JD[0]["Lines"].ToString());
            sf = liniasDialeg - 1;
            for (int z = 0; z < liniasDialeg; z++) {
                string TextPersona = "Persona_";
                string Text = "Line_";
                TextPersona += z;
                Text += z;
                Persona.Add(JD[0][TextPersona].ToString());
                Sequencia.Add(JD[0][Text].ToString());
            }
            loadFacesSprites();
        }
    }
    //carraguem les cares dels 2 que parlen
    private void loadFaces() {
        Face1 = (Texture2D)Resources.Load("Characters/Character1", typeof(Texture2D));
        Face2 = (Texture2D)Resources.Load("Characters/Character2", typeof(Texture2D));
        Face3 = (Texture2D)Resources.Load("Characters/Character3", typeof(Texture2D));
        Face4 = (Texture2D)Resources.Load("Characters/Character4", typeof(Texture2D));
        Face5 = (Texture2D)Resources.Load("Characters/Character5", typeof(Texture2D));
        Face6 = (Texture2D)Resources.Load("Characters/Character6", typeof(Texture2D));

        Speakers.Add(Persona[0]);
        if (Persona.Count > 1) {
            for (int i = 0; i < Persona.Count; i++) {
                int number_differences = 0;
                for (int j = 0; j < Speakers.Count; j++) {
                    if (Persona[i] != Speakers[j]) {
                        number_differences++;
                        if (number_differences == Speakers.Count) {
                            Speakers.Add(Persona[i]);
                        }
                    }
                }
            }
        }
    }

    private void loadFaces2() {
        Face1 = (Texture2D)Resources.Load("Characters/" + Persona[0], typeof(Texture2D));
        Speakers.Add(Persona[0]);
        if (Persona.Count > 1) {
            for (int i = 0; i < Persona.Count; i++) {
                int number_differences = 0;
                for (int j = 0; j < Speakers.Count; j++) {
                    if (Persona[i] != Speakers[j]) {
                        number_differences++;
                        if (number_differences == Speakers.Count) {
                            Speakers.Add(Persona[i]);
                        }
                    }
                }
            }
            if (Speakers.Count == 2) Face2 = (Texture2D)Resources.Load("Characters/" + Speakers[1], typeof(Texture2D));
            else if (Speakers.Count == 3) {
                Face2 = (Texture2D)Resources.Load("Characters/" + Speakers[1], typeof(Texture2D));
                Face3 = (Texture2D)Resources.Load("Characters/" + Speakers[2], typeof(Texture2D));
            }
            else if (Speakers.Count == 4) {
                Face2 = (Texture2D)Resources.Load("Characters/" + Speakers[1], typeof(Texture2D));
                Face3 = (Texture2D)Resources.Load("Characters/" + Speakers[2], typeof(Texture2D));
                Face4 = (Texture2D)Resources.Load("Characters/" + Speakers[3], typeof(Texture2D));
            }
        }
    }

    private void loadFacesSprites() {
        Face1S = Resources.Load("Characters/Character1", typeof(Sprite)) as Sprite;
        Face2S = Resources.Load("Characters/Character2", typeof(Sprite)) as Sprite;
        Face3S = Resources.Load("Characters/Character3", typeof(Sprite)) as Sprite;
        Face4S = Resources.Load("Characters/Character4", typeof(Sprite)) as Sprite;
        Face5S = Resources.Load("Characters/Character5", typeof(Sprite)) as Sprite;
        Face6S = Resources.Load("Characters/Character6", typeof(Sprite)) as Sprite;
        if (Persona.Count > 0) {
            Speakers.Add(Persona[0]);
            if (Persona.Count > 1) {
                for (int i = 0; i < Persona.Count; i++) {
                    int number_differences = 0;
                    for (int j = 0; j < Speakers.Count; j++) {
                        if (Persona[i] != Speakers[j]) {
                            number_differences++;
                            if (number_differences == Speakers.Count) {
                                Speakers.Add(Persona[i]);
                            }
                        }
                    }
                }
            }
        }
    }

    private void unLoadFaces() {
        Face1 = null;
        Face2 = null;
        Face3 = null;
        Face4 = null;
        Face5 = null;
        Face6 = null;
        Face1S = null;
        Face2S = null;
        Face3S = null;
        Face4S = null;
        Face5S = null;
        Face6S = null;
    }

    /* Guarda el string en cartells[0], el primer paramatre es per agafar el tipus de text, el segon per la subcatagoria
	*  Pot accedir a qualsevol lloc pero esta mes pensat per a accedir a System_Text per els dialegs de quan obres un cofre etc
	*/
    private void GetText(string typeText, string sysString) {
        for (int i = 0; i < textData[typeText].Count; i++) {
            if (textData[typeText][i]["name"].ToString() == sysString) {
                Cartells.Add(textData[typeText][i]["String"].ToString());
            }
        }
    }

    private void reactivecartellboto() {
        //posa la variable per a activar el cartell a true aixi al estar al collider s'activa i desde on estigui guardade es posara a fals al pulsar el boto de llegir
    }

    private void deactivatemov() {
        GameMaster.GM.setDialeg(true);//pot ser util per saber quan s'esta en dialeg o no
        GameMaster.GM.SetCanWalk (false);
        //desactiva el moviment del jugador
    }

    private void activatemov() {
        GameMaster.GM.setDialeg(false);
		GameMaster.GM.SetCanWalk (true);
		pintaYColoreaTextosCanvas();
        //reactiva el moviment del jugador
    }

    private void activaresperadialeg() {
        locksequencial = true;
    }

    private void desctivaresperadialeg() {
        locksequencial = false;
        dialegactiu = true;
        if (startdialeg) {
            corutine = nextdialeg();
            StartCoroutine(corutine);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "DP") {
            //GameObject.Find ("Player").GetComponent<PlayerControl>().setCanAction(true);
        }
    }
    private void OnTriggerStay2D(Collider2D other) {
        /*if (other.gameObject.tag == "DP" && GameObject.Find ("Player").GetComponent<PlayerControl>().isAction()==true) {
			if (GameObject.Find ("Player").GetComponent<PlayerControl> ().isAction ()) {
				GameObject.Find ("Player").GetComponent<PlayerControl>().setCanAction(false);
				if (cartell&&!startdialeg){
					activarCartell();
				}
				if (isdialeg&&!startdialeg){
					activarsequencia();
				}
			}
		}*/
    }


    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "DP") {
            //GameObject.Find ("Player").GetComponent<PlayerControl>().setCanAction(false);
            //GameObject.Find ("Player").GetComponent<PlayerControl> ().setAction (false);
        }
    }

    private void colorAndFace() {
        if (si < Persona.Count) {
            if (Persona[si] == "Character2") {
                //drawface = true;
                Face = Face2;
                //colorfont = new Color32(202, 108, 57, 255);
            }
            else if (Persona[si] == "Character1") {
                //drawface = true;
                Face = Face1;
                //colorfont = new Color(255f, 214f, 0f);
            }
            else if (Persona[si] == "Character3") {
                //drawface = true;
                Face = Face3;
                //colorfont = new Color32(255, 51, 51, 255);
            }
            else if (Persona[si] == "Character4") {
                //drawface = true;
                Face = Face4;
                //colorfont = new Color32(255, 51, 51, 255);
            }
            else if (Persona[si] == "Character5") {
                //drawface = true;
                Face = Face5;
                //colorfont = Color.white;
            }
            else {
                //drawface = true;
                Face = Face1;
                //colorfont = Color.white;
            }
        }
        else {
            //drawface = false;
            //colorfont = Color.white;
        }
    }

    public void activarCartell() {//activa la part dels cartells
        blabla = Cartells[0];
        dialegactiu = true;
        toggleDialeg();
    }

    public void activarsequencia() { //activar la variable sequencia
        dialegactiu = true;
        sequencia = true;
        if (dialegnpc) si = 0;
        blabla = Sequencia[si];
        toggleDialeg();         //activa el lock del player
    }

    public void activarCartell(string _nomString) {//activa la part dels cartells
        pista = false;
        Cartells.Clear();
        Persona.Clear();
        Speakers.Clear();
        string zone = DataBase.db.getZone();
        string language = DataBase.db.getLanguageStr();
        TextAsset textdata = Resources.Load("Texts/" + language + "/" + zone + "/Objectes") as TextAsset;
        textData = JsonMapper.ToObject(textdata.ToString());
        GetText("System_Text", _nomString);
        loadFacesSprites();

        blabla = Cartells[0];
        dialegactiu = true;
        toggleDialeg();
    }

    public void examineItem(string _nomString) {//activa la part dels cartells
        pista = false;
        Cartells.Clear();
        Persona.Clear();
        Speakers.Clear();
        string language = DataBase.db.getLanguageStr();
        TextAsset textdata = Resources.Load("Texts/" + language + "/Items/Objectes") as TextAsset;
        textData = JsonMapper.ToObject(textdata.ToString());
        GetText("System_Text", _nomString);
        loadFacesSprites();

        blabla = Cartells[0];
        dialegactiu = true;
        toggleDialeg();
    }

    public void activarsequencia(string _NPC) { //activar la variable sequencia
        pista = false;
        endsequencia = false;
        Sequencia.Clear();
        Persona.Clear();
        Speakers.Clear();
        string zone = DataBase.db.getZone();
        string language = DataBase.db.getLanguageStr();
        TextAsset textdata = Resources.Load("Texts/" + language + "/" + zone + "/Converses") as TextAsset;
        textData = JsonMapper.ToObject(textdata.ToString());
        populateLists(_NPC);

        dialegactiu = true;
        sequencia = true;
        si = 0;
        blabla = Sequencia[si];
        toggleDialeg();         //activa el lock del player
    }

    public void activarPistaPuzzle(string _pista) { //activar la variable sequencia
        pista = true;
        endsequencia = false;
        Sequencia.Clear();
        Persona.Clear();
        Speakers.Clear();
        string zone = DataBase.db.getZone();
        string language = DataBase.db.getLanguageStr();
        TextAsset textdata = Resources.Load("Texts/" + language + "/" + zone + "/Pista") as TextAsset;
        textData = JsonMapper.ToObject(textdata.ToString());
        populateLists(_pista);

        dialegactiu = true;
        sequencia = true;
        si = 0;
        blabla = Sequencia[si];
        toggleDialeg();         //activa el lock del player
    }

    public void activarPistaPuzzle(string _pista, string nom) { //activar la variable sequencia
        endsequencia = false;
        Sequencia.Clear();
        Persona.Clear();
        Speakers.Clear();
        string zone = DataBase.db.getZone();
        string language = DataBase.db.getLanguageStr();
        TextAsset textdata = Resources.Load("Texts/" + language + "/" + zone + "/Pista") as TextAsset;
        textData = JsonMapper.ToObject(textdata.ToString());
        populateLists(_pista);
        Persona[0] = nom;

        pista = true;
        dialegactiu = true;
        sequencia = true;
        si = 0;
        blabla = Sequencia[si];
        toggleDialeg();         //activa el lock del player
    }

    public void StopDialeg() {
        Sequencia.Clear();
        Persona.Clear();
        Speakers.Clear();
        pista = false;
        dialegactiu = false;
        sequencia = false;
        startdialeg = false;
        mirror = false;
        skip = false;
    }

    public void SkipNo() {
        if(corutine != null) StopCoroutine(corutine);
    }
    public void restartDialeg() {
        canskip = false;
        skip = false;
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
            corutine = nextdialegMobile();
            StartCoroutine(corutine);
        }
        else {
            corutine = nextdialeg();
            StartCoroutine(corutine);
        }
    }
}
