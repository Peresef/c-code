﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;

public class DataBase : MonoBehaviour {
#if UNITY_IOS
#else
    [DllImport("__Internal")]
    private static extern void SyncFiles();

    [DllImport("__Internal")]
    private static extern void WindowAlert(string message);
#endif

    public static DataBase db;
    public int _numberSeqBib;
    public int _numberSeqArx;

    private bool _seqPuzzle = false;
	private int _language = 0; //0 catala 1 castella 2 angles
	private bool _Dug = false;
    private bool[] _sequenceBiblo;
    private bool[] _sequenceArxiu;
    private string _zone = "Menu";
    private bool[] _intro = new bool[6];
	private List<Item> _inventory = new List<Item>();
	private bool[] _Doors = new bool[4];//0 Biblioteca-Arxiu 1 Biblioteca-Passadis 2 Colummnes-Passadis 3 Columnas-exterior 
	private bool [] _Puzzle = new bool[4];//diria que son 5, però si en tot cas ja ho canviarem
	private bool[] _Objects = new bool[5];//0 carrito 1 llibre 2 pala 3 part clau 4 partclau2
	private bool historiador = false; //ens dira si el historiador esta desbloquejat
                                      //0 arxiu 1 paraninf 2 observatori 3 columns 4 general/secret
    private bool[] _saves = new bool[2];
    private bool[] _end= new bool[4];// 0 bool per saber si es pot ejecutar els finals, 1 per el end 1, 3 per el end 2
    private bool[] _zoneName = new bool[6];

    // Use this for initialization
    void Awake () {
        if (db == null){
			for (int i = 0; i <_Objects.Length; i++){
				_Objects[i]=true;
			}
            DontDestroyOnLoad(gameObject);
            db = this;
        }
        else if (db != this){
            Destroy(gameObject);
        }
		_inventory.Capacity = 7;
        _sequenceArxiu = new bool[_numberSeqArx];
        _sequenceBiblo = new bool[_numberSeqBib];
        for (int i = 0; i < _numberSeqBib; i++) {
            _sequenceBiblo[i] = false;
        }
        for (int i = 0; i < _numberSeqArx; i++) {
            _sequenceArxiu[i] = false;
        }
        for (int i = 0; i < _saves.Length; i++) {
            _saves[i] = false;
        }
        for (int i = 0; i < _end.Length; i++) {
            _end[i] = false;
        }
    }

	void Start(){

	}
	// Update is called once per frame
	void Update () {
		
	}

    public void setZone(string zone) {
        _zone = zone;
    }
    
    public string getZone() {
        return _zone;
    }

    public void setIntro(bool intro) {
        int i;
        if (_zone == "Biblioteca") {
            i = 0;
        }
        else if (_zone == "Arxiu") {
            i = 1;
        }
        else if (_zone == "Passadis") {
            i = 2;
        }
        else if (_zone == "Observatori") {
            i = 3;
        }
        else if (_zone == "Paraninf") {
            i = 4;
        }
        else if (_zone == "Hall") {
            i = 5;
        }
        else return;
        _intro[i] = intro;
    }
    public void setIntro(int i, bool intro) {
        _intro[i] = intro;
    }

    public bool getIntro() {
        int i;
        if (_zone == "Biblioteca") {
            i = 0;
            return _intro[i];
        }
        else if (_zone == "Arxiu") {
            i = 1;
            return _intro[i];
        }
        else if (_zone == "Passadis") {
            i = 2;
            return _intro[i];
        }
        else if (_zone == "Observatori") {
            i = 3;
            return _intro[i];
        }
        else if (_zone == "Paraninf") {
            i = 4;
            return _intro[i];
        }
        else if (_zone == "Hall") {
            i = 5;
            return _intro[i];
        }
        else return false;
    }

    public bool getIntro(int i) {
        if (i == 0) {
            return _intro[i];
        }
        else if (i== 1) {
            return _intro[i];
        }
        else if (i == 2) {
            return _intro[i];
        }
        else if (i == 3) {
            return _intro[i];
        }
        else if (i == 4) {
            return _intro[i];
        }
        else if (i == 5) {
            return _intro[i];
        }
        else return false;
    }

    public void setIntroZ(bool intro) {
        int i;
        if (_zone == "Biblioteca") {
            i = 0;
        }
        else if (_zone == "Arxiu") {
            i = 1;
        }
        else if (_zone == "Passadis") {
            i = 2;
        }
        else if (_zone == "Observatori") {
            i = 3;
        }
        else if (_zone == "Paraninf") {
            i = 4;
        }
        else if (_zone == "Hall") {
            i = 5;
        }
        else return;
        _zoneName[i] = intro;
    }

    public void setIntroZ(int i, bool intro) {
        _zoneName[i] = intro;
    }

    public bool getIntroZ() {
        int i;
        if (_zone == "Biblioteca") {
            i = 0;
            return _zoneName[i];
        }
        else if (_zone == "Arxiu") {
            i = 1;
            return _zoneName[i];
        }
        else if (_zone == "Passadis") {
            i = 2;
            return _zoneName[i];
        }
        else if (_zone == "Observatori") {
            i = 3;
            return _zoneName[i];
        }
        else if (_zone == "Paraninf") {
            i = 4;
            return _zoneName[i];
        }
        else if (_zone == "Hall") {
            i = 5;
            return _zoneName[i];
        }
        else return false;
    }

    public bool getIntroZ(int i) {
        if (i == 0) {
            return _zoneName[i];
        }
        else if (i == 1) {
            return _zoneName[i];
        }
        else if (i == 2) {
            return _zoneName[i];
        }
        else if (i == 3) {
            return _zoneName[i];
        }
        else if (i == 4) {
            return _zoneName[i];
        }
        else if (i == 5) {
            return _zoneName[i];
        }
        else return false;
    }

    public bool getEnd(int pos) {
        return _end[pos];
    }

    public void setEnd(int pos, bool state) {
        _end[pos] = state;
    }

    public void setSequence(int pos, bool _state) {
        if (_zone == "Biblioteca") {
            _sequenceBiblo[pos] = _state;
        }
        if (_zone == "Arxiu") {
            _sequenceArxiu[pos] = _state;
        }
    }

    public bool getSequence(int pos) {
        if (_zone == "Biblioteca") {
            return _sequenceBiblo[pos];
        }
        if (_zone == "Arxiu") {
            return _sequenceArxiu[pos];
        }
        else return false;
    }

    public int getNumberSeq() {
        if (_zone == "Biblioteca") {
            return _numberSeqBib;
        }
        if (_zone == "Arxiu") {
            return _numberSeqArx;
        }
        else return 0;
    }

    public void setHistoriador(){
		historiador = true;
	}
    public void setHistoriador(bool _state) {
        historiador = _state;
    }

    public bool getHistoriador(){
		return historiador;
	}
	//Inventory Functions
	public void AddItem(Item _item){
		_inventory.Add (_item);
		if(getZone()!="Menu") RefreshInventory ();
	}

	public void RemoveItem(int _slot){
		_inventory.RemoveAt (_slot);
		RefreshInventory ();
	}
	public int ItemCount(){
		return _inventory.Count;
	}

	public int ItemPos(Item _item){
		for (int i = 0; i < _inventory.Count; i++) {
			if (CheckItem (_inventory [i], _item)) {
				return i;
			}
		}
		return 0;
	}
	public void RefreshInventory(){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Sprite imatge;
		for (int i = 0; i <_inventory.Capacity-1; i++) {//clean all the slots drawing a void sprite on it
            Image imageCanvas = canvasObject.transform.Find("Inventory/Slot"+(i+1)).GetComponent<Image>();
            imatge = Resources.Load("Items/empty", typeof(Sprite)) as Sprite;
			imageCanvas.sprite = imatge;
		}
		for(int i =0; i<_inventory.Count; i++){//draw the respective sprite
            Image imageCanvas = canvasObject.transform.Find("Inventory/Slot"+(i+1)).GetComponent<Image>();
            imatge = Resources.Load("Items/"+_inventory[i]._Name, typeof(Sprite)) as Sprite;
			imageCanvas.sprite = imatge;
		}	
	}

	public Item UseItem(int _item){
		//Debug.Log (_item);
		return _inventory [_item];
	}

	public void ExaminateItem(int _item){
		Dialogos.TM.examineItem(_inventory[_item]._Name);
	}
	//per mira si un objecte esta al inventori
	public bool CheckInventory(string name){
		for (int i= 0; i < ItemCount();i++){
			if(CheckItem(name, _inventory[i].name))return true;
		}
		return false;
	}
	/// <summary>
	/// funcions per a les claus del paraninf i observatori
	/// </summary>

	public bool CheckEspecial(){
		for (int i= 0; i < ItemCount();i++){
			if(_inventory[i].especial)return true;
		}
		return false;
	}

	public void RemoveEspecial(){
		for (int i= 0; i < ItemCount();i++){
            if (_inventory[i].especial) {
                RemoveItem(i);
            }
		}
	}

	public void checkEspecialAdd(int num){
		if(!CheckEspecial()){
			Item item = Resources.Load("Items/PartClau"+num, typeof(Item)) as Item;
			AddItem (item);
		}
		else{
			RemoveEspecial();
			Item item = Resources.Load("Items/ClauFinal", typeof(Item)) as Item;
			AddItem (item);
			Dialogos.TM.activarCartell("KeyComplete");
		}
		//sino afegim la clau sencera i eliminem el objecte que tenim
	}

	private bool CheckItem(string i1, string i2){
		return i1==i2;
	}

	private  bool CheckItem( Item _i1, Item _i2){
		return _i1._Name == _i2._Name;
	/*	if (_i1._Name == _i2._Name) {
			return true;
		} else {
			return false;
		}*/
	}

    private void saveInventory(SaveData data) {
        string[] _saveInventory = new string[7];
        for (int i=0; i<_inventory.Count;i++) {
            _saveInventory[i] = _inventory[i]._Name;
        }
        data._inventory = _saveInventory;
    }

    private void loadInventory(SaveData data) {
        string[] _saveInventory = new string[7];
        _saveInventory = data._inventory;
        for (int i = 0; i < _saveInventory.Length; i++) {
            if (_saveInventory[i] != "") {
                Item item = Resources.Load("Items/" + _saveInventory[i], typeof(Item)) as Item;
                Debug.Log(item);
                AddItem(item);
            }
        }
    }

    //End of Inventory Functions

    //Doors functions
    /// <summary>
    /// Funcio per a obrir una porta, utiliza el convert door per passar de string a int(per no guardar un
    /// array de strings que ocupara més memoria que un de bools
    /// </summary>

    public void OpenDoor(string door){
		_Doors[ConvertDoor(door)] = true;
	}

	/// <summary>
	/// Mirem si la porta esta oberta o no
	/// Tenim la versio amb string i amb int, la d'string es mes per l'script door
	/// </summary>

	public bool CheckDoor(string door){
		return _Doors[ConvertDoor(door)];
	}

	public bool CheckDoor(int door){
		return _Doors[door];
	}

    public void setDoors(int id, bool _state) {
        _Doors[id] = _state;
    }
	//no se si són els noms adequats,mirar-ho
	private int ConvertDoor(string door){
		switch (door){
		case "Biblioteca-Arxiu":
			return 0;
		case "Biblioteca-Hall":
			return 1;
		case "Hall-Passadis":
			return 2;
		case "End":
			return 3;
		}
		return -1;
	}
	/// <summary>
	/// per saber si la porta es realment no important o no hi ha d'haver interaccio amb ella
	/// </summary>
	/// <returns><c>true</c>, if important was checked, <c>false</c> otherwise.</returns>
	/// <param name="door">Door.</param>
	public bool CheckImportant(string door){
		if(door =="Biblioteca-Arxiu")return true;
		else if(door=="Biblioteca-Hall")return true;
		else if(door=="Hall-Passadis")return true;
		else if(door=="End")return true;
		return false;
	}
	//End Doors Functions

	//Puzzle Functions
	public void EndPuzzle(int id){//com que imagino que els finals de cada puzzle sera hardcoded no crec que faci falta cap manera pro de diferenciar
		_Puzzle[id] = true;
	}

	public bool CheckPuzzle(int id){
		return _Puzzle[id];
	}

    public void setPuzzle(int id, bool _state) {
        _Puzzle[id] = _state;
    }
	/// <summary>
	/// funcio que ens servira per a saber per quina part del joc passa el jugador
	/// </summary>
	/// <returns>retorna un string amb el nom d'on passa(anotacio: pere no se ben be el string que ha de retornar, aqui ho canvies segons vegis).</returns>
	public string getPartOfGame(){
		int acabats = 0;
		int portes = 0;
		for(int i = 0; i < _Puzzle.Length; i++){
			if(_Puzzle[i])acabats++;
		}
		if(acabats>2)portes=acabats-1;//per quadrar les portes amb el puzzle
		else portes = acabats;
		return convertName(acabats) + "_" + CheckDoor(portes).ToString();
		//en teoria hauria de retornar una cosa similar a aquesta. Biblioteca_false. Voldria dir que esta a la biblioteca i no ha obert la porta de l'arxiu
		//una altre opcio seria Hall_false. Voldria dir que ja ha passat el puzzle del llibre, però encara no ha obert la porta de la biblioteca cap al passadís (la porta de l'arxiu es dona per fet que esta oberta)
		//Per ultim un altre exemple seria Hall_true, en aquest cas s'ha acabat el puzzle del llibre i s'ha obert la porta


		//Els noms seran els que tu vegis que son els adequats
	}

	private string convertName(int id){
		switch(id){
		case 0:
			return "Biblioteca";
		case 1: 
			return "Passadis";
		case 2://voldra dir que s'ha fet el puzzle del paraninf o el del observatori
			if(_Puzzle[1])return "Observatori";//vol dir que ha completat el paraninf i va per l'observatori
			else if(_Puzzle[2])return "Paraninf";//vol dir que s'ha fet el observatori i s'ha de fer el paraninf
			break;
		case 3:
			return "Hall";
		case 4:
			return "End";
		}
		return "";
	}


	//End Puzzle Functions


	///Objects Functions///
	public bool CheckObject(int id){
		return _Objects[id];
	}

	public void SetObjects(int id){
		_Objects[id] = false;
	}

    public void SetObjects(int id, bool _state) {
        _Objects[id] = _state;
    }

	public bool getObject(int id){
		return _Objects [id];
	}
    /////Functions dug
    //funcions per a saber si la variable de excavat esta activa o no
    public void setDug(bool boolea){
		_Dug = boolea;
	}

	public bool getDug(){
		return _Dug;
	}

    public bool getSaves (int pos) {
        return _saves[pos];
    }

    public void setSaves(int pos) {
        _saves[pos] = true; 
    }

    ////language functions
    public void setLanguage(int option){
		_language = option;
	}

	public string getLanguageStr(){
        if (_language == 0) return "catala";
        else if (_language == 1) return "castella";
        else if (_language == 2) return "angles";
        else return "error";
	}

    public int getLanguage() {
        return _language;
    }

    public void setSeqPuzzle(bool _state) {
        _seqPuzzle = _state;
    }

    public bool getSeqPuzzle() {
        return _seqPuzzle;
    }

    public void reset() {
        for (int i = 0; i < _Objects.Length; i++) {
            _Objects[i] = true;
        }
        _inventory.Clear();
        setZone("Biblioteca");
        setHistoriador(false);
        setDug(false);
        setSeqPuzzle(false);
        for (int i = 0; i < 6; i++) setIntro(i, false);
        for (int i = 0; i < 6; i++) setIntroZ(i, false);
        for (int i = 0; i < 4; i++) setDoors(i, false);
        for (int i = 0; i < 4; i++) setPuzzle(i, false);
        for (int i = 0; i < 5; i++) SetObjects(i, true);
        for (int i = 0; i < 4; i++) setEnd(i, false);
    }

    public void Save() {
        string dataPath = string.Format("{0}/GameDetails.dat", Application.persistentDataPath);
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream fileStream;

        try {
            if (File.Exists(dataPath)) {
                File.WriteAllText(dataPath, string.Empty);
                fileStream = File.Open(dataPath, FileMode.Open);
            }
            else {
                fileStream = File.Create(dataPath);
            }

            SaveData data = new SaveData();

            data._seqPuzzle = _seqPuzzle;
            data._language = _language;
            data._Dug = _Dug;
            data._sequenceBiblo = _sequenceBiblo;
            data._sequenceArxiu = _sequenceArxiu;
            data._zone = _zone;
            data._intro = _intro;
            data._Doors = _Doors;
            data._Puzzle = _Puzzle;
            data._Objects = _Objects;
            data.historiador = historiador;
            //saveInventory(data);
            data.saves = _saves;
            data._zoneName = _zoneName;


            binaryFormatter.Serialize(fileStream, data);
            fileStream.Close();
#if UNITY_IOS
#else
            if (Application.platform == RuntimePlatform.WebGLPlayer) {
                SyncFiles();
        }
#endif
        }
        catch (Exception e) {
#if UNITY_IOS
#else
            PlatformSafeMessage("Failed to Save: " + e.Message);
#endif
        }

        }

    public bool Load() {
        string dataPath = string.Format("{0}/GameDetails.dat", Application.persistentDataPath);

       try {
            if (File.Exists(dataPath)) {

                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(dataPath, FileMode.Open);

                SaveData data = (SaveData)bf.Deserialize(file);
                file.Close();

                //loadInventory(data);
                _seqPuzzle = data._seqPuzzle;
                _language = data._language;
                _Dug = data._Dug;
                _sequenceBiblo = data._sequenceBiblo;
                _sequenceArxiu = data._sequenceArxiu;
                _zone = data._zone;
                _intro = data._intro;
                _Doors = data._Doors;
                _Puzzle = data._Puzzle;
                _Objects = data._Objects;
                historiador = data.historiador;
                _saves = data.saves;
                _zoneName = data._zoneName;

                return true;
            }
       }
       catch (Exception e) {
#if UNITY_IOS
#else
           PlatformSafeMessage("Failed to Load: " + e.Message);
           return false;
#endif
       }
        return false;
    }
#if UNITY_IOS
#else
    private static void PlatformSafeMessage(string message) {
        if (Application.platform == RuntimePlatform.WebGLPlayer) {
            WindowAlert(message);
        }
        else {
            Debug.Log(message);
        }
    }
#endif

    public void Print() {
        for (int i = 0; i < _Objects.Length; i++) {
            Debug.Log("Objects " + i + " : " + _Objects[i]);
        }
        Debug.Log("Zone: "+getZone());
        Debug.Log("Histo: "+getHistoriador());
        Debug.Log("Dug: "+getDug());
        Debug.Log("Seq puzzle: "+getSeqPuzzle());
        for (int i = 0; i < 6; i++) Debug.Log("Intro "+i+" : "+getIntro(i));
        for (int i = 0; i < 4; i++) Debug.Log("Door " + i + " : "+CheckDoor(i));
        for (int i = 0; i < 4; i++) Debug.Log("Puzzle " + i + " : "+CheckPuzzle(i));
        for (int i = 0; i < 5; i++) Debug.Log("Objects" + i + " : "+getObject(i));
        for (int i = 0; i < 4; i++) Debug.Log("End " + i + " : "+getEnd(i));
    }
}

[Serializable]
class SaveData {
    public bool _seqPuzzle;
    public int _language; //0 catala 1 castella 2 angles
    public bool _Dug;
    public bool[] _sequenceBiblo;
    public bool[] _sequenceArxiu;
    public string _zone;
    public bool[] _intro = new bool[6];
    public bool[] _zoneName = new bool[6];
    public string[] _inventory = new string[7];
    public bool[] _Doors = new bool[4];//0 Biblioteca-Arxiu 1 Biblioteca-Passadis 2 Colummnes-Passadis 3 Columnas-exterior 
    public bool[] _Puzzle = new bool[4];//diria que son 5, però si en tot cas ja ho canviarem
    public bool[] _Objects = new bool[5];//0 carrito 1 llibre 2 pala 3 part clau 4 partclau2
    public bool historiador;
    public bool[] saves = new bool[2];
}