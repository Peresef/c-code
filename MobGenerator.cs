﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MobGenerator : MonoBehaviour {
	public enum State{
		Idle,
		Initialize,
		Setup,
		SpawnMob
	}
	public int timespawn=120;
	public float distancespawn=1f;
	public GameObject[] mobPrefabs;
	public GameObject[] spawnPoints;
	public State state; 
	public float scaleY = 1;
	public float scaleX = 1;
	public bool faceright=false;
	public float DistanceEndPathX=2.726f;
	public float DistanceEndPathY=0;
	public float Deteccio;
	public float FireDelayEyeGear;
	
	private bool canspawn=false;
	private bool nospawn=false;
	private Transform _pc;
	private Player pl;
	private int timer = 0;
	
	void Awake(){
		state = State.Initialize;
	}
	// Use this for initialization
	IEnumerator Start () {
		while (true) {
			switch(state){
			case State.Initialize:
				Initialize();
				break;
			case State.Setup:
				Setup();
				break;
			case State.SpawnMob:
				SpawnMob();
				break;
			case State.Idle:
				Idle();
				break;
			}
			
			yield return 0;
		}
	}
	
	private void Initialize(){
		if (!CheckForMobPrefabs())
			return;
		if (!CheckForSpawnPoints())
			return;
		
		
		state = State.Setup;
	}
	
	private void Setup(){
		state = State.SpawnMob;
	}
	
	private void SpawnMob(){
		GameObject[] gos = AvaliableSpawnPoints ();
		//	Debug.Log (gos);
		if (canspawn == true) {
			for (int cnt =0; cnt< gos.Length; cnt++) {
				GameObject go = Instantiate (mobPrefabs [Random.Range (0, mobPrefabs.Length)],
				                             gos [cnt].transform.position,
				                             Quaternion.identity
				                             ) as GameObject;
				go.transform.parent = gos [cnt].transform;
				go.transform.localScale = new Vector3 (scaleX, scaleY, 1);
				if (faceright == true && go.gameObject.tag == "Serp") {
					go.transform.eulerAngles = new Vector2 (0, 0);
				}
				if (faceright == true && go.gameObject.tag == "Spindula") {
					go.transform.eulerAngles = new Vector2 (0, 180);
				}
				if (go.gameObject.tag == "Serp" || go.gameObject.tag == "Fantasma" || go.gameObject.tag == "Spindula") {
					go.gameObject.SendMessage ("targetflagmob");
					if (go.gameObject.tag == "Serp") {
						Deteccioshooting ds = go.GetComponent<Deteccioshooting> ();
						ds.deteccio = Deteccio;
					}
				} else if (go.gameObject.tag == "eyeGearStatic") {
					Targetting1 ta1 = go.gameObject.GetComponentInChildren<Targetting1> ();
					ta1.flag = true;
					EyeGear eg = go.gameObject.GetComponentInChildren<EyeGear> ();
					eg.fireDelay = FireDelayEyeGear;
				} else if (go.gameObject.tag == "MothGothPH") {
					Transform child = go.gameObject.transform.GetChild (1).GetChild (1);
					child.localPosition = new Vector3 (DistanceEndPathX, DistanceEndPathY, 0);
				} else if (go.gameObject.tag == "eyeGearPath") {
					Targetting1 ta1 = go.gameObject.GetComponentInChildren<Targetting1> ();
					ta1.flag = true;
					Transform child = go.gameObject.transform.GetChild (0).GetChild (1);
					child.localPosition = new Vector3 (DistanceEndPathX, DistanceEndPathY, 0);
					EyeGear eg = go.gameObject.GetComponentInChildren<EyeGear> ();
					eg.fireDelay = FireDelayEyeGear;
				}
			}
			canspawn = false;
			state = State.Idle;
		}
	}
	
	private void Idle(){
		if (GameObject.Find ("Player") != null) {
			pl = GameObject.Find ("Player").GetComponent<Player> ();
			if(pl.notrans==0){ 
				_pc=pl.personatge.transform;
			}
		}
		if (GameObject.Find ("Player_trans") != null) {
			pl = GameObject.Find ("Player_trans").GetComponent<Player> ();
			if (pl.notrans == 1) {
				_pc = pl.p_trans.transform;
			}
		}
		GameObject[] gos = AvaliableSpawnPoints ();
		if (gos.Length != 0) {
			timer++;
			for (int cnt =0; cnt< gos.Length; cnt++) {
				if(gos[cnt].transform.position.x-distancespawn < _pc.transform.position.x && _pc.transform.position.x<gos[cnt].transform.position.x+distancespawn)
					nospawn=true;
				else nospawn=false;
			}
			if(timer > timespawn && !nospawn){
				timer=0;
				state = State.Initialize;
			}
		}
		
	}
	
	private bool CheckForMobPrefabs(){
		if (mobPrefabs.Length > 0)
			return true;
		else
			return false;
	}
	
	private bool CheckForSpawnPoints(){
		if (spawnPoints.Length > 0)
			return true;
		else
			return false;
	}
	
	private GameObject[] AvaliableSpawnPoints(){
		List<GameObject> gos = new List<GameObject> ();
		
		for (int cnt=0; cnt <spawnPoints.Length; cnt++) {
			if(spawnPoints[cnt].transform.childCount==0){
				gos.Add(spawnPoints[cnt]);
			}
		}
		return gos.ToArray();
	}
	
	private void respawnmob(){
		canspawn = true;
		StartCoroutine (stopspawn ());
		
	}
	
	private IEnumerator stopspawn(){
		yield return new WaitForSeconds (0.8f);
		canspawn = false;
		state = State.Idle;
	}
}

